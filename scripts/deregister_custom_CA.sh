# read in certificateId
echo "read in certificateId"
certificateId=$(cat .server_certs/certificateId.txt)
echo $certificateId
# deregister
echo "deregister the root certificate"
aws iot update-ca-certificate --certificate-id $certificateId --new-status INACTIVE --region eu-central-1
aws iot delete-ca-certificate --certificate-id $certificateId  --region eu-central-1
# delete .server_certs folder
echo "clean and remove the .server_certs folder"
rm -rf .server_certs