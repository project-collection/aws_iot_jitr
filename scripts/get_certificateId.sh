#!/bin/bash

# Exit if any of the intermediate steps fail
set -e

certificateId=$(cat .server_certs/certificateId.txt)
jq -n --arg certificateId "$certificateId" '{"certificateId":$certificateId}'
