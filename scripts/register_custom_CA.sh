#! /usr/local/bin


function register_custom_CA {
    # ensure .server_certs directory exists
    mkdir -p .server_certs
    # create root CA
    openssl genrsa -out .server_certs/sampleCACertificate.key 2048
    # automated non-interactive openssl https://www.shellhacks.com/create-csr-openssl-without-prompt-non-interactive/
    openssl req -x509 -new -nodes -key .server_certs/sampleCACertificate.key -sha256 -days 365 -out .server_certs/sampleCACertificate.pem -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=example.com"
    # get iot registration code
    export registration_code=$(jq -r '.registrationCode' <<< $(aws iot get-registration-code --region eu-central-1))
    # use the registration code to create a CSR
    # https://aws.amazon.com/de/blogs/iot/just-in-time-registration-of-device-certificates-on-aws-iot/
    openssl genrsa -out .server_certs/privateKeyVerification.key 2048
    openssl req -new -key .server_certs/privateKeyVerification.key -out .server_certs/privateKeyVerification.csr -subj "/O=Global Security/OU=IT Department/CN=$registration_code"
    # Now that you have a CSR that includes the registration code, use your first sample CA certificate and the CSR to create a verification certificate
    openssl x509 -req -in .server_certs/privateKeyVerification.csr -CA .server_certs/sampleCACertificate.pem -CAkey .server_certs/sampleCACertificate.key -CAcreateserial -out .server_certs/privateKeyVerification.crt -days 365 -sha256
    # use the verification certificate to register your sample CA certificate
    export certificateId=$(jq -r '.certificateId' <<< $(aws iot register-ca-certificate --ca-certificate file://.server_certs/sampleCACertificate.pem --verification-certificate file://.server_certs/privateKeyVerification.crt --region eu-central-1))
    # persist certificateId
    echo $certificateId > .server_certs/certificateId.txt 
    aws iot describe-ca-certificate --certificate-id $certificateId --region eu-central-1
    # register the CA certificate in the ACTIVE state
    aws iot update-ca-certificate --certificate-id $certificateId --new-status ACTIVE --region eu-central-1
    # register the CA certificate with the auto-registration-status enabled 
    aws iot update-ca-certificate --certificate-id $certificateId --new-auto-registration-status ENABLE --region eu-central-1
}

# clean the environment for new certification installation

# when should the register process be started
# SUCCESS signal file is not available
if [ -f ".server_certs/SUCCESS" ]; then
    echo "root certificate is already registered"
else
    echo "create and register a new certificate"
    register_custom_CA
    if [ $? -eq 0 ]; then
        touch .server_certs/SUCCESS
        echo "certificate successfully created and registered"
    else
        echo "register custom CA to AWS IoT failed, please clean the .server_certs/ dir and try it again"
        exit 1
    fi
fi