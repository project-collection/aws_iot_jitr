resource "null_resource" "register_custom_CA" {
  triggers = {
    # trigger the register_custom_CA.sh every time
    build_number = "${timestamp()}"
  }

  provisioner "local-exec" {
    command = "bash scripts/register_custom_CA.sh"
  }
}

resource "null_resource" "deregister_custom_CA" {
  provisioner "local-exec" {
    when    = destroy
    command = "bash scripts/deregister_custom_CA.sh"
  }
}


# device activate lambda
resource "aws_iam_role" "deviceActivation" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "deviceActivation" {
  statement {
    sid = "1"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
     "arn:aws:logs:*:*:*",
    ]
  }

  statement {
    sid = "2"
    effect = "Allow"
    actions = [
      "iot:UpdateCertificate",
      "iot:CreatePolicy",
      "iot:AttachPrincipalPolicy"
    ]
    resources = [
      "*",
    ]
  }
}


resource "aws_iam_policy" "deviceActivation" {
  name   = "deviceActivation_policy"
  path   = "/"
  policy = data.aws_iam_policy_document.deviceActivation.json
}

resource "aws_iam_role_policy_attachment" "deviceActivation" {
  role       = aws_iam_role.deviceActivation.name
  policy_arn = aws_iam_policy.deviceActivation.arn
}

# attach policy to lambda

# source code https://github.com/aws-samples/aws-iot-examples/blob/master/justInTimeRegistration/deviceActivation.js
resource "aws_lambda_function" "deviceActivation" {
  filename      = "${path.module}/deviceActivation.zip"
  function_name = "deviceActivation"
  role          = aws_iam_role.deviceActivation.arn
  handler       = "deviceActivation.handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = filebase64sha256("${path.module}/deviceActivation.zip")
  runtime = "nodejs12.x"
}

data "external" "certificateId" {
  program = ["bash", "${path.module}/scripts/get_certificateId.sh"]
  depends_on = [null_resource.register_custom_CA]
}

data "aws_caller_identity" "current" {}


# Creating an AWS Lambda Rule
resource "aws_iot_topic_rule" "deviceActivation" {
  name        = "deviceActivation"
  description = "deviceActivation"
  enabled     = true
  sql         = "SELECT * FROM '$aws/events/certificates/registered/${data.external.certificateId.result["certificateId"]}'"
  sql_version = "2016-03-23"

  lambda {
    function_arn = aws_lambda_function.deviceActivation.arn
  }

  # give "call lambda" permission to iot service 
  provisioner "local-exec" {
        command = "aws lambda add-permission --function-name $function_name --region eu-central-1 --principal iot.amazonaws.com --source-arn $rule_arn --source-account $source_account --statement-id Id-123 --action \"lambda:InvokeFunction\""
        environment = {
          function_name = aws_lambda_function.deviceActivation.function_name
          rule_arn = aws_iot_topic_rule.deviceActivation.arn
          source_account = data.aws_caller_identity.current.account_id
        }
  }

  tags = {
    test="test"
  }
}