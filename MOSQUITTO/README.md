### START MOSQUITTO CONFIG

1. Add "cert.crt", "private.key" and "rootCA.pem" to /MOSQUITTO/certs
2. Run "docker build -t "mqtt" ." from ./MOSQUITTO
3. Run "docker run -it -p 8883:8883 mqtt"
