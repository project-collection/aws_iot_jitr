# AWS IoT Just In Time Registration

Auto register the device certificate which is signed by pre-registered CA certificate.

## Requirements

* installed terraform binary
* installed jq
* installed awscli (default installed on ec2 amazon linux)
* should have rights to deploy all necessary resources within the account
* installed docker daemon
* installed mosquitto client


## How to Set Requirements

1. start EC2 with Amazon Linux 2

2. install docker CE
```
sudo amazon-linux-extras install docker
sudo service docker start
sudo usermod -a -G docker ec2-user

# Make docker auto-start
sudo chkconfig docker on

# Reboot to verify it all loads fine on its own.
sudo reboot
```

3. install terraform
```
wget https://releases.hashicorp.com/terraform/0.12.28/terraform_0.12.28_linux_amd64.zip
unzip terraform_0.12.28_linux_amd64.zip
sudo mv terraform /usr/local/bin/terraform
```

4. install jq
```
sudo yum install jq -y
```

5. install mosquitto
```
sudo amazon-linux-extras install epel
sudo yum -y install mosquitto
sudo systemctl start mosquitto
sudo systemctl enable mosquitto
```

## Documentation

* About Server Certification

located in folder .server_certs/ , naming after blog https://aws.amazon.com/de/blogs/iot/just-in-time-registration-of-device-certificates-on-aws-iot/

* About Device Client

You will see a TLS failure when you run the command because AWS IoT disconnects the connection after the registration of the device certificate. AWS IoT has registered the certificate in the PENDING_ACTIVATION state and won’t let it connect and authenticate unless the certificate is marked ACTIVE. The client or device should implement an automatic reconnect strategy when it is disconnected due to the TLS error. The device should also implement a back-off strategy (that is, increase the time between retries) to avoid unnecessary network traffic and allow your own activation logic to complete before devices attempt to reconnect. (https://aws.amazon.com/de/blogs/iot/just-in-time-registration-of-device-certificates-on-aws-iot/)


## How to Deploy

Deploy JITR solution with terraform into account, in region eu-central-1

```
# initialize plugins etc
terraform init
# check the change on aws
terraform plan
# execute the change
terraform apply
```

You can tear down the whole infrastructure with

```
terraform destroy
```

## How to Test

create device certificates

```bash
# create device certificateyes
openssl genrsa -out deviceCert.key 2048
openssl req -new -key deviceCert.key -out deviceCert.csr
openssl x509 -req -in deviceCert.csr -CA .server_certs/sampleCACertificate.pem -CAkey .server_certs/sampleCACertificate.key -CAcreateserial -out deviceCert.crt -days 365 -sha256
# Create a certificate file that contains the device certificate and its registered CA certificate.
cat deviceCert.crt .server_certs/sampleCACertificate.pem > deviceCertAndCACert.crt
```

Test

```bash
# Test connection (optional)
# https://docs.aws.amazon.com/iot/latest/developerguide/diagnosing-connectivity-issues.html
# openssl s_client -connect a3vh46e1syeqth-ats.iot.us-east-1.amazonaws.com:8443 -CAfile root.cert -cert deviceCertAndCACert.crt -key deviceCert.key

# mosquitto_pub --cafile root.cert --cert deviceCertAndCACert.crt --key deviceCert.key -h <prefix>-ats.iot.us-east-1.amazonaws.com -p 8883 -q 1 -t  foo/bar -i  anyclientID --tls-version tlsv1.2 -m "Hello" -d
# prefix can be got with aws iot describe-endpoint, be careful of the region in endpoint

mosquitto_pub --cafile root.cert --cert deviceCertAndCACert.crt --key deviceCert.key -h a3vh46e1syeqth-ats.iot.eu-central-1.amazonaws.com -p 8883 -q 1 -t  topic/test -i  clientId1 --tls-version tlsv1.2 -m "Hello" -d

# have to run twice, first one for the registration and will fail, second one publish the message
mosquitto_pub --cafile root.cert --cert deviceCertAndCACert.crt --key deviceCert.key -h a3vh46e1syeqth-ats.iot.eu-central-1.amazonaws.com -p 8883 -q 1 -t  topic/test -i  clientId1 --tls-version tlsv1.2 -m "Hello" -d
```

clean after test

```bash
rm deviceCert.key deviceCert.csr deviceCert.crt deviceCertAndCACert.crt
```

The root.cert is the AWS IoT root certificate. The AWS IoT root CA certificate is used by a device to verify the identity of the AWS IoT servers. Click here to download the root certificate. (https://www.amazontrust.com/repository/AmazonRootCA1.pem)

auto-registration lambda will create device cerficiate on IoT and policy on certificate, which should be deleted manually (not through terraform) if needed



## Reference

* What Are CA Certificates? https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2003/cc778623(v=ws.10)?redirectedfrom=MSDN
* Use Your Own Certificate with AWS IoT https://aws.amazon.com/de/blogs/mobile/use-your-own-certificate-with-aws-iot/
* Just-in-Time Registration of Device Certificates on AWS IoT https://aws.amazon.com/de/blogs/iot/just-in-time-registration-of-device-certificates-on-aws-iot/
* Provisioning with a bootstrap certificate in AWS IoT Core https://aws.amazon.com/de/blogs/iot/provisioning-with-a-bootstrap-certificate-in-aws-iot-core/
* Digital Certificates: Chain of Trust https://www.youtube.com/watch?v=heacxYUnFHA
* Configure the first connection by a client for automatic registration: https://docs.aws.amazon.com/iot/latest/developerguide/auto-register-device-cert.html#configure-auto-reg-first-connect
* How to Bridge Mosquitto MQTT Broker to AWS IoT: https://aws.amazon.com/de/blogs/iot/how-to-bridge-mosquitto-mqtt-broker-to-aws-iot/